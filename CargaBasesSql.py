
#Cargar paquetes y servidores
import pandas as pd
from sqlalchemy import create_engine
import re
import time
import pyodbc
import io
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
from googleapiclient.http import MediaIoBaseDownload
import pickle
import os
import datetime
import cx_Oracle
import json
import requests
import numpy as np

#Conexion al servidor de oracle
con = cx_Oracle.connect('ro_user/ro_user@//Oracle.avalchile.cl:1521/ORCL')
conSC = cx_Oracle.connect('PGC_CL/PGC_CL@//Oracle.avalchile.cl:1521/ORCL')
#Conexion al servidor local con pyodbc
connStr = pyodbc.connect("""DRIVER={ODBC Driver 13 for SQL Server};
                            SERVER=AVALCHWS212\SQLEXPRESS;
                            DATABASE=ControlGestion;
                            Trusted_Connection=yes""")

cursor = connStr.cursor()

#Conexion a los servidores con sqlalchemy
sqlServer = create_engine("mssql+pyodbc://python:python@SqlServer")
mysql = create_engine("mysql+pymysql://userdbdatastudio:userdbdatastudio@avalch-mysql.cjih4mheomsq.us-west-2.rds.amazonaws.com:3306/datastudio",isolation_level="READ UNCOMMITTED")

#%%
#######################################################
#Funciones de apoyo
######################################################
def toFechas(column): #Funcion para encontrar columnas con formato fecha y parsearlas
    #Busca formato fecha en todas las columnas. usar cuando el input del excel es solamente texto
    try:
        column=pd.to_datetime(column,format = '%d/%m/%y')
    except:
        try:
            if column.dtype.num==17:
                column=pd.to_datetime(column,format = '%Y-%m-%d')
            else:    
                print('columna'+' '+column.name+' '+'no puede ser transformada en tipo TimeStamp')
        except:
            print('columna'+' '+column.name+' '+'no puede ser transformada en tipo TimeStamp')
            
    return(column)

def VaciosaNone(df): #Funcion para buscar nan y nat dentro de un dataframe y convertirlos en vacios
    #Cambia los nan y NaT a Vacios, no sirve el pd.where para cambiar los NaT a Vacios
    df=df.apply(lambda x: x.apply(lambda y: '' if y != y else y ),axis=0)
    df=df.where((pd.notnull(df)),'')
    return(df)


def TruncarCeldas(celda): #try catch de apoyo para incorporar dentro del apply en la funcion "Truncar"
    try:
        if type(celda) == str:
            if len (celda) > 250:
                celda=celda[:250]
            else:
                celda=celda
    except:
        pass
    return(celda)

def Truncar(df):#Busca strings con largo mayor a 250 para evitar errores por varchar(250) en sql
    #Truncar los textos muy largos
    df=df.apply(lambda x: x.apply(TruncarCeldas),axis=1)
    return(df)    

def eliminarExprCelda(celda,expr,replace): #try catch de apoyo para incorporar dentro del apply en la funcion "eliminarExpr"
    try:
        expr=re.compile(str(expr))
        celda=re.sub(expr,replace,celda)
    except:
        pass
    return(celda)
    
def eliminarExpr(df,expr,replace): #Busca dentro de un dataframe una expresion y la reemplaza
    df=df.apply(lambda x: x.apply(eliminarExprCelda,args=(expr,replace)))
    return(df)

def insertSql(df,connStr,tabla,batch=1000): #Funcion para insertar un dataframe en una tabla especifica. Utilizar cuando falle sqlalchemy
    df=df.reset_index()                 #
    try:                                #
        df=df.drop("index",axis=1)      # REINICIAR EL INDICE DEL DATAFRAME
    except:                             #
        df=df                           #
        
    if batch > 1000:
        batch = 1000
    cursor = connStr.cursor()
    #Columnas para el insert
    columnasInsert=''
    for names in df.columns.values:
        columnasInsert+='['+names+']'+','
    columnasInsert=columnasInsert[:-1]
    
    start_time = time.time()
    rows=''
    for index,row in df.iterrows():
        #Insertar data de a mil filas
        for names in df.columns.values:
            if row[names]=='':
                rows+='NULL'+',' #Agregar los vacios en el formato que corresponde
                if names==df.columns.values[len(df.columns.values)-1]: 
                   rows=rows[:-1]
            else:
                rows+="'"+str(row[names])+"'"+',' #Insertar los datos
                if names==df.columns.values[len(df.columns.values)-1]:
                    rows=rows[:-2]+"'"
        rows=rows+'),('
        if ((index+1)%batch)==0:
            rows=rows[:-3]
            print('Se han insertado ' + str(index+1)+ ' datos a '+str(tabla))
            print('Han transcurrido ' + str(time.time() - start_time)+' segundos')
            cursor.execute('INSERT INTO '+tabla+'('+columnasInsert+')values('+rows+')')
            connStr.commit()
            rows=''
        elif((index+1)==len(df)):
            rows=rows[:-3]
            print('Se han insertado ' + str(index+1)+ ' datos a '+str(tabla))
            print('Han transcurrido ' + str(time.time() - start_time)+' segundos')
            cursor.execute('INSERT INTO '+tabla+'('+columnasInsert+')values('+rows+')')
            connStr.commit()
            rows=''        

def makeService(SCOPES): #Se crea un objeto con coneccion a google drive
    creds = None
    # The file token.pickle stores the user's access and refresh tokens, and is
    # created automatically when the authorization flow completes for the first
    # time.
    if os.path.exists('token.pickle'):
        with open('token.pickle', 'rb') as token:
            creds = pickle.load(token)
    # If there are no (valid) credentials available, let the user log in.
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(
                'credentials.json', SCOPES)
            creds = flow.run_local_server()
        # Save the credentials for the next run
        with open('token.pickle', 'wb') as token:
            pickle.dump(creds, token)

    service = build('drive', 'v3', credentials=creds)
    return(service)
    
    


def retrieve_all_files(service):
  """Retrieve a list of File resources.

  Args:
    service: Drive API service instance.
  Returns:
    List of File resources.
  """
  result = []
  page_token = None
  while True:
    try:
      param = {}
      if page_token:
        param['pageToken'] = page_token
      files = service.files().list(**param).execute()

      result.extend(files['files'])
      page_token = files.get('nextPageToken')
      if not page_token:
        break
    except:
      print ('An error occurred')
      break
  return result

#%%
#########################################################################
#Cargar la informacion al servidor local
###########################################################################################################################################################
#Capital expuesto seguros de credito
#Obtener los datos originales
HoraActual=datetime.datetime.now()
print('Cargando Cap Exp a las '+str(HoraActual))
query = 'select * from [ControlGestion].[dbo].[SegCred_Capital_Expuesto_Acum]'
dfOriginal = pd.read_sql(query,sqlServer)
#nombres y tipos que correspondes
ColumnNames=dfOriginal.columns.values
ColumnTypes=dfOriginal.dtypes

#Data a agregar
df = pd.read_excel('//avalstorage/Presentación Comercial/Capital Expuesto/Copia de Capital Expuesto Carga.xlsx')
#df = pd.read_excel('//avalstorage/Presentación Comercial/Capital Expuesto/octubre2018.xlsx')
#Seleccionar las columnas de la base de datos
df=df[df.columns.intersection(ColumnNames)]
#Mes desde el cual eliminar los meses
mes = df["FECHA_PROCESO"][0][3:5]
anho = df["FECHA_PROCESO"][0][6:8]
FechaDeleteQueryCapitalExpuesto = '20'+anho+'-'+mes+'-'+'01'
query='delete from [ControlGestion].[dbo].[SegCred_Capital_Expuesto_Acum] where [FECHA_PROCESO] >= ' + "'"+FechaDeleteQueryCapitalExpuesto+"'"
#query='delete from [ControlGestion].[dbo].[SegCred_Capital_Expuesto_Acum] where [FECHA_PROCESO] >= ' + "'"+FechaDeleteQueryCapitalExpuesto+"' and [FECHA_PROCESO] < '2018-11-01' "
cursor.execute(query)
print('Se borraron los datos del mes' + ' ' + mes + ' anho ' + anho)
df=df.apply(toFechas)
df=df.where(df != '-',0)
df=df.where(df != "'",' ')
df=Truncar(df)
df=eliminarExpr(df,"'",' ')
df=VaciosaNone(df)
#Subir al servidor local
HoraActual=datetime.datetime.now()
print('Subiendo Cap Exp al servidor local a las '+str(HoraActual))
tabla='SegCred_Capital_Expuesto_Acum'
try:
    insertSql(df,connStr,tabla)
except:
    print('Fallo carga ' + str(tabla))
#df.to_sql('SegCred_Capital_Expuesto_Acum',sqlServer,chunksize=10000,if_exists='append',index=False)
print('Se subieron los datos al cap exp')
########################################################################################################################################################

########################################################################################################################################################
#Produccion acumulada garantias tecnicas
#Datos originales
HoraActual=datetime.datetime.now()
print('Cargando Prod Acum GT a las '+str(HoraActual))
query = 'select * from [ControlGestion].[dbo].[SegGarantia_Libro_Produccion]'
dfOriginal = pd.read_sql(query,sqlServer)
#nombres y tipos que correspondes
ColumnNames=dfOriginal.columns.values
ColumnTypes=dfOriginal.dtypes
#Data a agregar
df=pd.read_excel('//192.168.1.8/ArchivosDW/SegurosGarantia/Libro acumulado.xlsx',sheet_name="Hoja1")
#Seleccionar las columnas de la base de datos
df.rename(columns={'Tomador':'Nombre','Prima Bruta Pesos':'Prima Pesos'},inplace=True)
df=df[df.columns.intersection(ColumnNames)]

#Formato que corresponde
df["Tasa"]=df["Tasa"].where(df["Tasa"].apply(lambda x: x != 'PM'),999)
df=df.where(df != '-',0)
df=df.where(df != "'",' ')
df=Truncar(df)
df=eliminarExpr(df,"'",' ')
df=VaciosaNone(df)
#Borrar los datos antiguos
cursor.execute('Delete FROM dbo.SegGarantia_Libro_Produccion')
connStr.commit()
#Subir los datos
HoraActual=datetime.datetime.now()
print('Subiendo Prod Acum GT a las '+str(HoraActual))
tabla='dbo.SegGarantia_Libro_Produccion'
try:
    insertSql(df,connStr,tabla)
except:
    print('Fallo carga: '+str(tabla))
print('Se cargo el libro acumulado GT')
########################################################################################################################################################

########################################################################################################################################################
#Parametros para conectarse a google drive
os.chdir("C:/Users/jjvalenzuela.AVALCHILE/Documents/Python Scripts/ETLGoogleDrive")
SCOPES = ['https://www.googleapis.com/auth/drive']
service= makeService(SCOPES)
files=retrieve_all_files(service)
for archivos in files:
    if archivos['name']=='2016-2018  Libro de Producción acumulada (Cr).xlsx':
        SheetidCR=archivos['id']
    if archivos['name']=='2016-2018  Libro de Producción acumulada (G).xlsx':
        SheetidG=archivos['id']    
######################################################################################
HoraActual=datetime.datetime.now()
print('Cargando Prod Acum GT Peru a las '+str(HoraActual))
#Libro de produccion G de Peru
#Descargar el archivo
file_id = SheetidG
request = service.files().get_media(fileId=file_id)
filename='LpPeruG.xlsx'
fh = io.FileIO(filename, 'wb')
downloader = MediaIoBaseDownload(fh, request)
done = False
while done is False:
    status, done = downloader.next_chunk()
    print("Download %d%%." % int(status.progress() * 100))
fh.close()

#Data original
query = 'select * from [ControlGestion].[dbo].[Peru_SegGarantia_Libro_Produccion_Historico]'
dfOriginal = pd.read_sql(query,sqlServer)
#nombres y tipos que correspondes
ColumnNames=dfOriginal.columns.values
#crear columna FechaCierre
#Se le eliminan 5 dias dado que pueden entregar el cierre del mes en esos 5 dias
hoy=datetime.datetime.today()
hoy=hoy-datetime.timedelta(3)
FechaCierre=datetime.datetime(hoy.year,hoy.month,1)

df=pd.read_excel('C:/Users/jjvalenzuela.AVALCHILE/Documents/Python Scripts/ETLGoogleDrive/LpPeruG.xlsx',sheet_name='Detalle')
df=df[0:sum(df["Grupo Económico"].notnull())] #PARA ELIMINAR LOS COMENTARIOS AL FINAL DEL EXCEL
df.rename(columns={'N° Fianza':'Póliza','Sol/Renov.':'Sol/Renov#','Ftermino':'FTérmino'},inplace=True)
#Agregar fecha cierre
df["FechaCierre"]=str(FechaCierre)
#formato
df=df[df.columns.intersection(ColumnNames)]
df=df.where(df != '-',0)
df=df.where(df != "'",' ')
df=Truncar(df)
df=eliminarExpr(df,"'",' ')
df=VaciosaNone(df)
df["Tasa"]=df["Tasa"].where(df["Tasa"].apply(lambda x: x != 'PM'),999)
df=df.astype(str)
df=df[df.columns.intersection(ColumnNames)]
#Borrar los datos antiguos
cursor.execute('Delete FROM dbo.Peru_SegGarantia_Libro_Produccion_Historico where FechaCierre >= '+"'"+str(FechaCierre)+"'")
connStr.commit()
#Subir los datos
HoraActual=datetime.datetime.now()
print('Subiendo Prod Acum GT Peru a las '+str(HoraActual))
tabla='dbo.Peru_SegGarantia_Libro_Produccion_Historico'
try:
    insertSql(df,connStr,tabla)
except:
    print('fallo carga: '+str(tabla))
print('Se cargo el libro de produccion g de peru')
#######################################################################################
HoraActual=datetime.datetime.now()
print('Cargando Prod Acum Seg Cred Peru a las '+str(HoraActual))
#Libro de produccion CR de Peru
#Descargar el archivo
file_id = SheetidCR
request = service.files().get_media(fileId=file_id)
filename='LpPeruCr.xlsx'
fh = io.FileIO(filename, 'wb')
downloader = MediaIoBaseDownload(fh, request)
done = False
while done is False:
    status, done = downloader.next_chunk()
    print("Download %d%%." % int(status.progress() * 100))
fh.close()

#Data original
query = 'select * from [ControlGestion].[dbo].[Peru_SegCred_Libro_Produccion]'
dfOriginal = pd.read_sql(query,sqlServer)
#nombres y tipos que correspondes
ColumnNames=dfOriginal.columns.values

#Leer el archivo
df=pd.read_excel('C:/Users/jjvalenzuela.AVALCHILE/Documents/Python Scripts/ETLGoogleDrive/LpPeruCr.xlsx',sheet_name='Libro de Producción')
#formato
df.rename(columns={'Sol/Renov.':'Sol/Renov#','Periodo Asegurado':'Período Asegurado','O.I.T. (20%)':'O.I.T (20%)'},inplace=True)
df=df.where(df != '-',0)
df=df.where(df != "'",' ')
df=Truncar(df)
df=eliminarExpr(df,"'",' ')
df=VaciosaNone(df)
df["Tasa"]=df["Tasa"].where(df["Tasa"].apply(lambda x: x != 'PM'),999)
df["Sol/Renov#"]=df["Sol/Renov#"].where(df["Sol/Renov#"].apply(lambda x: x != 's1' ),0)
df["Sol/Renov#"]=df["Sol/Renov#"].where(df["Sol/Renov#"].apply(lambda x: x != 's2' ),0)
df=df.astype(str)
df=df[df.columns.intersection(ColumnNames)]

#Borrar los datos antiguos
cursor.execute('Delete FROM dbo.Peru_SegCred_Libro_Produccion')
connStr.commit()
#Subir los datos
HoraActual=datetime.datetime.now()
print('Subiendo Prod Acum Seg Cred Peru a las '+str(HoraActual))
tabla='dbo.Peru_SegCred_Libro_Produccion'

try:
    insertSql(df,connStr,tabla)
except:
    print('Fallo la carga de: '+str(tabla))

print('Se cargo el libro de produccion CR de Peru')


############################################################################################################################

############################################################################################################################
#Posicion Financiamiento Final
HoraActual=datetime.datetime.now()
print("Cargando Base Financiamiento a las " + str(HoraActual) )
df=pd.read_excel('//avalstorage/Credito/Financiamiento/Posición Financiamiento Final.xlsm',sheet_name='BASE',header=3,index_col=[0,19,20,21,22,23])
df=df[0:sum(df['N° Poliza'].notnull())]
tabla='dbo.Financiamiento'
df = df.reset_index(drop=True)
df=VaciosaNone(df)
df=df.where(df != '-',0)
#borrar datos antiguos
print("Borrando datos de" + " " + tabla)
cursor.execute('Delete FROM ' + tabla)
connStr.commit()
try:
    insertSql(df,connStr,tabla)
except:
    print('Fallo la carga de: '+str(tabla))
    
HoraActual=datetime.datetime.now()
print("Se cargo la base de financiamiento a las " + str(HoraActual))

###############################################################################################################################
#Base tipos de cambio
HoraActual=datetime.datetime.now()
print("Cargando base tipos de cambio a las " + str(HoraActual))
############################
#Base monedas servidor amaru
############################
HoraActual=datetime.datetime.now()
print("Extrayendo datos de amaru a las " + str(HoraActual))
query=""" select * from PGC_CL.Tscr_Tasas_Cambios"""
df = pd.read_sql(query,conSC)
df["CDTIPO_MONEDA"]=df["CDTIPO_MONEDA"].astype(int)

Codigos=pd.DataFrame({"DSTIPO_MONEDA":[220,110,214,108,109],"MONEDA":["DOLARES","EURO","UF","UF2","UTM"]})

df=pd.merge(df,Codigos,how="left",left_on="CDTIPO_MONEDA",right_on="DSTIPO_MONEDA")
df=df[["FETASA","MONEDA","TASACAMBIO"]]


df=df[df["FETASA"]>=np.datetime64('2019-01-01')]

Monedas=["DOLARES","EURO","UF","UTM"]
DataMonedasTSCR={"Periodo":[],"DOLARES":[],"EURO":[],"UF":[],"UTM":[]}

for i in df["FETASA"].unique():
    DataMonedasTSCR["Periodo"].append(i)
    for j in Monedas:
       try:
           DataMonedasTSCR[j].append(float(df[(df["FETASA"]==i) & (df["MONEDA"]==j)]["TASACAMBIO"].values))
       except:
           DataMonedasTSCR[j].append("")

DataMonedasTSCR=pd.DataFrame(DataMonedasTSCR).sort_values(by=["Periodo"])

####################################
#Base sol bcentral peru
HoraActual=datetime.datetime.now()
print("Extrayendo datos de Banco Central de Peru a las " + str(HoraActual))
###################################
datos = json.loads(requests.get('https://estadisticas.bcrp.gob.pe/estadisticas/series/api/PD04641PD/json/', verify = False).text)
datos=pd.DataFrame(datos["periods"])
for i in range(0,len(datos["values"])): #Sacar el valor de la lista y colocarlo como numero
    datos["values"][i]=float(datos["values"][i][0])

#Formatear el mes para convertirlo en datetime
datos["name"]=datos["name"].str.replace("Ene","01")
datos["name"]=datos["name"].str.replace("Feb","02")
datos["name"]=datos["name"].str.replace("Mar","03")
datos["name"]=datos["name"].str.replace("Abr","04")
datos["name"]=datos["name"].str.replace("May","05")
datos["name"]=datos["name"].str.replace("Jun","06")
datos["name"]=datos["name"].str.replace("Jul","07")
datos["name"]=datos["name"].str.replace("Ago","08")
datos["name"]=datos["name"].str.replace("Set","09")
datos["name"]=datos["name"].str.replace("Oct","10")
datos["name"]=datos["name"].str.replace("Nov","11")
datos["name"]=datos["name"].str.replace("Dic","12")

datos["name"]=pd.to_datetime(datos["name"],format="%d.%m.%y")
datos=datos.rename(columns={'values':'Sol'})

#Unificar bases
DataMonedasTSCR=DataMonedasTSCR[DataMonedasTSCR["Periodo"]<=max(datos["name"])]
DataMonedas=DataMonedasTSCR.merge(datos,how="left",left_on="Periodo",right_on="name")[["Periodo","DOLARES","EURO","UF","UTM","Sol"]]
DataMonedas["Sol"][pd.isna(DataMonedas["Sol"])]=""

######################################
#Cargar a base local
######################################
query = 'select * from [ControlGestion].[dbo].[Tipo_Cambio]'
dfOriginal = pd.read_sql(query,sqlServer)

DataMonedas=DataMonedas[DataMonedas["Periodo"]>max(dfOriginal["Periodo"])] #Definir datos a cargar
DataMonedas=DataMonedas.rename(columns={'EURO':'EUR',"Sol":"SOL/USD","DOLARES":"USD"})
DataMonedas["SOL/CLP"]=""

df=DataMonedas

tabla='dbo.Tipo_Cambio'
HoraActual=datetime.datetime.now()
print("Subiendo los datos a la tabla " + tabla + " a las " + str(HoraActual))

try:
    insertSql(df,connStr,tabla)
except:
    print('Fallo la carga de: '+str(tabla))


############
#Cerrar la coneccion 
cursor.close()
connStr.close()

#%%
#Subir data a servidor de amazon
HoraActual=datetime.datetime.now()
print('Cargando servidor local al servidor de amazon a las '+str(HoraActual))
##########
#MOVIMIENTOS HISTORICOS SGR
query = 'select * from MovimientosHistoricosSGR'
df = pd.read_sql(query,sqlServer)
df.to_sql('MovimientosHistoricosSGR',mysql,chunksize=10000,if_exists='replace',index=False)
##########
##########
#Visitas SGR
query = 'select * from VW_VisitasSGR'
df = pd.read_sql(query,sqlServer)
df.to_sql('VisitasSGR',mysql,chunksize=10000,if_exists='replace',index=False)
##########
##########
#Operaciones Cursadas SAGR
query = 'select * from VW_SGR_CUADRATURA_BD'
df = pd.read_sql(query,sqlServer)
df.to_sql('SGR_CUADRATURA_BD',mysql,chunksize=10000,if_exists='replace',index=False)
##########
####
#Libro de produccion
query = 'select * from VW_ComiteComercial_garantias'
df = pd.read_sql(query,sqlServer)
df.to_sql('Comite_GarantiasT_LP',mysql,chunksize=10000,if_exists='replace',index=False)

query = 'select * from VW_ComiteComercial_Clientes'
df = pd.read_sql(query,sqlServer)
df.to_sql('Comite_GarantiasT_ClientesNuevos',mysql,chunksize=10000,if_exists='replace',index=False)

query = 'select * from VW_ComiteComercial_PrimaGarantias'
df = pd.read_sql(query,sqlServer)
df.to_sql('Comite_GarantiasT_Prima',mysql,chunksize=10000,if_exists='replace',index=False)

#Fuente
query = 'select * from SegGarantia_Libro_Produccion'
df = pd.read_sql(query,sqlServer)
df.to_sql('Comite_GarantiasT_LibroProduccionFuente',mysql,chunksize=10000,if_exists='replace',index=False)
#####

######
#Garantias tecnicas Peru
query = 'select * from VW_ComiteComercial_Peru_Cauciones'
df = pd.read_sql(query,sqlServer)
df.to_sql('Comite_Cauciones_Resumen',mysql,chunksize=10000,if_exists='replace',index=False)
########

####
#Libro prod seg cred peru
query = 'select * from VW_ComiteComercial_Peru_SegCredito'
df = pd.read_sql(query,sqlServer)
df.to_sql('Comite_CreditoPeru_Resumen',mysql,chunksize=10000,if_exists='replace',index=False)
####
#Seg credito
query = 'select * from VW_ComiteComercial_Financiamiento_CapVigente'
df = pd.read_sql(query,sqlServer)
df.to_sql('Comite_Financiamiento_CapVigente',mysql,chunksize=10000,if_exists='replace',index=False)

query = 'select * from VW_ComiteComercial_SegCredito'
df = pd.read_sql(query,sqlServer)
df.to_sql('Comite_Credito_CapExpuesto',mysql,chunksize=10000,if_exists='replace',index=False)

query = 'select * from VW_ComiteComercial_SegCredito_Primas'
df = pd.read_sql(query,sqlServer)
df.to_sql('Comite_Credito_Primas',mysql,chunksize=10000,if_exists='replace',index=False)

query = 'select * from VW_ComiteComercial_SegCredito_Clientes'
df = pd.read_sql(query,sqlServer)
df.to_sql('Comite_Credito_Clientes',mysql,chunksize=10000,if_exists='replace',index=False)

#####
#FINANCIAMIENTO
query = 'select * from VW_ComiteComercial_Financiamiento'
df = pd.read_sql(query,sqlServer)
df.to_sql('Comite_Financiamiento',mysql,chunksize=10000,if_exists='replace',index=False)

query = 'select * from VW_ComiteComercial_Financiamiento_Resumen'
df = pd.read_sql(query,sqlServer)
df.to_sql('Comite_Financiamiento_Resumen',mysql,chunksize=10000,if_exists='replace',index=False)

#Financiamiento Ingetal
query = 'select * from VW_ComiteComercial_Financiamiento_Resumen_INGETAL'
df = pd.read_sql(query,sqlServer)
df.to_sql('Comite_Financiamiento_Resumen_INGETAL',mysql,chunksize=10000,if_exists='replace',index=False)
###

####
#  Reporte riesgo Garantias financieras
query = 'select * from VW_SGR_LogOperaciones'
df = pd.read_sql(query,sqlServer)
df.to_sql('Comite_GarantiasF_Riesgo',mysql,chunksize=10000,if_exists='replace',index=False)
###
###########################
#   Para subir el pipedrive
query = 'select * from VW_PipeActivities'
df = pd.read_sql(query,sqlServer)
df.to_sql('Comite_GarantiasT_PipeActivities',mysql,chunksize=10000,if_exists='replace',index=False)

query = 'select * from VW_PipeDeals'
df = pd.read_sql(query,sqlServer)
df.to_sql('Comite_GarantiasT_PipeDeals',mysql,chunksize=10000,if_exists='replace',index=False)

# informacion capital mercado publico
query = 'select * from VW_ComiteComercial_CapitalMercadoPublico'
df = pd.read_sql(query,sqlServer)
df.to_sql('Comite_GarantiasT_PipeDealsCapitalMercadoPublico',mysql,chunksize=10000,if_exists='replace',index=False)
########################

########################
#CARGAR TIPO DE CAMBIO
query = 'select * from VW_Tipo_Cambio_Fill'
df = pd.read_sql(query,sqlServer)
df.to_sql('TipoDeCambio',mysql,chunksize=10000,if_exists='replace',index=False)
######################

#################
#CLIENTES NUEVOS AMARU
query = 'select * from VW_Amaru_ClientesNuevos'
df = pd.read_sql(query,sqlServer)
df.to_sql('ClientesNuevosAmaru',mysql,chunksize=10000,if_exists='replace',index=False)
####################

###################
#EJECUTIVOS SGR
query = 'select * from SGR_log_operaciones_ejecutivos'
df = pd.read_sql(query,sqlServer)
df.to_sql('EjecutivosSGR',mysql,chunksize=10000,if_exists='replace',index=False)
###########################

##########################
#Reporte garantias financieras riesgos
query = 'select * from VW_MovimientosHistoricosSGR'
df = pd.read_sql(query,sqlServer)
df.to_sql('MovimientosHistoricosSGR',mysql,chunksize=10000,if_exists='replace',index=False)
#Tabla Cumplimiento SGR
query = 'select * from CumplimientoSGR'
df = pd.read_sql(query,sqlServer)
df.to_sql('CumplimientoSGR',mysql,chunksize=10000,if_exists='replace',index=False)
####################################

##################################
#Cumplimiento tarifario
query = 'select * from VW_CumplimientoTarifario'
df = pd.read_sql(query,sqlServer)
df.to_sql('CumplimientoTarifario',mysql,chunksize=10000,if_exists='replace',index=False)


#%%
#IMPORTAR DATA RESOLUCIÓN RIESGOS GARANTIAS TÉCNICAS
query = """SELECT
TSGE_MINUTA.ID_MINUTA,
CASE WHEN TSGE_MINUTA.COD_ESTADO = 'AP' THEN 'APROBADA'
      WHEN TSGE_MINUTA.COD_ESTADO = 'PR' THEN 'PREREGISTRADA'
      WHEN TSGE_MINUTA.COD_ESTADO = 'RE' THEN 'REGISTRADA'
      WHEN TSGE_MINUTA.COD_ESTADO = 'PE' THEN 'PENDIENTE DE APROBACION'
      WHEN TSGE_MINUTA.COD_ESTADO = 'RC' THEN 'RECHAZADA'
      WHEN TSGE_MINUTA.COD_ESTADO = 'OB' THEN 'OBSERVADA'
      WHEN TSGE_MINUTA.COD_ESTADO = 'CE' THEN 'CERRADA'
      ELSE 'INACTIVA'
END AS ESTADO_MINUTA,
MONEDA.DSTIPO_MONEDA,
NOM_CARGO,
PGC_CL.TSGE_MINUTA.USU_MODIFICA,
TSGE_MINUTA.VAL_CONTRATO,
MINDET.VAL_OTORGADO AS VALOR_OTORGADO,
NVL(mincob.PORCENTAJE_TASA, 0) AS TASA,
--mincob.COD_TIPO_COBERTURA as COBERTURA,
catcob.NOM_DATO as COBERTURA,
TOM.DSNOMBRES AS TOMADOR,
CASE WHEN UPPER(USU.COD_USUARIO) IS NOT NULL THEN UPPER(USU.COD_USUARIO)
ELSE TSGE_MINUTA.USU_REGISTRA END AS SUSCRIPTOR,
CASE WHEN TSGE_MINUTA.COD_TIPO_MODALIDAD = 'LI' THEN 'LINEA'
      WHEN TSGE_MINUTA.COD_TIPO_MODALIDAD = 'OP' THEN 'OPERACION PUNTUAL'
      ELSE TSGE_MINUTA.COD_TIPO_MODALIDAD END AS TIPO_MODALIDAD,
(CASE 
 WHEN TSGE_MINUTA.COD_TIPO_MINUTA = 'S' THEN 'SUSCRIPCION' 
 WHEN TSGE_MINUTA.COD_TIPO_MINUTA = 'C' THEN 'COMERCIAL' 
 ELSE 'RAVLA' END ) TIPO_MINUTA,
NVL(NIVEL.NOM_NIVEL_APROBACION, 'MIGRADO') AS NIVEL_APROBACION,
NVL(TSGE_MINUTA.ID_PROYECTO, 'N/A') AS PROYECTO,
--TO_CHAR(MINDET.FEC_INICIO_VIGENCIA, 'DD/MM/YYYY') AS INICIO_VIGENCIA,
--TO_CHAR(MINDET.FEC_FIN_VIGENCIA, 'DD/MM/YYYY') AS FIN_VIGENCIA,
--MINDET.NUM_DIAS AS DIAS_PROYECTO,
--NVL(MINDET.PORCENTAJE_TASA, 0) AS TASA,
NVL(TSGE_MINUTA.PORCENTAJE_FACULTATIVO, 0) AS PCT_FACULTATIVO_OPERACION,
TSGE_MINUTA.USU_REGISTRA,
TSGE_MINUTA.COD_TIPO_CANAL,
--,(SELECT FEC_ACEPTACION FROM TSGE_PROPUESTA WHERE ID_PROPUESTA = )
TO_CHAR(TSGE_MINUTA.FEC_REGISTRA, 'dd/MM/yyyy HH:MI:SS AM') AS FEC_REGISTRA,
--TO_CHAR(TSGE_MINUTA.FEC_REGISTRA, 'dd/MM/yyyy HH:MI:SS AM') AS FEC_EMISION,
TO_CHAR(NVL(TSGE_MINUTA.FEC_MODIFICA, SYSDATE), 'dd/MM/yyyy HH:MI:SS AM') AS FEC_MODIFICA,
TO_CHAR(NVL(TSGE_MINUTA.FEC_RESOLUCION, SYSDATE), 'dd/MM/yyyy HH:MI:SS AM') AS FEC_RESOLUCION
--TSGE_MINUTA.MATERIA_ASEGURADA,
--TSGE_MINUTA.DESCRIP_COMENTARIO
FROM PGC_CL.TSGE_MINUTA
INNER JOIN PGC_CL.TSGE_MINUTA_DETALLE MINDET ON MINDET.ID_MINUTA = TSGE_MINUTA.ID_MINUTA
left join PGC_CL.TSGE_MINUTA_COBERTURA mincob on mincob.ID_MINUTA = TSGE_MINUTA.ID_MINUTA
left join PGC_CL.TSGE_CATALOGO_GENERAL catcob on (mincob.COD_TIPO_COBERTURA = catcob.COD_DATO and catcob.NOM_GRUPO = 'TIPO_COBERTURA') 
 --LEFT JOIN TCUM_TOMADORES TOM ON MINU.NMSEC_DNI_TOMADOR = TOM.NMSEC_DNI_TOMADOR
LEFT JOIN PGC_CL.TSCR_PERSONAS TOM ON PGC_CL.TSGE_MINUTA.NMSEC_DNI_TOMADOR = TOM.NMSEC_DNI
--LEFT JOIN PGC_PE.TCUM_CONSORCIOS CON ON MINU.NMCONSORCIO = CON.NMCONSORCIO
LEFT JOIN PGC_CL.TSGE_USUARIO USU ON TSGE_MINUTA.ID_USUARIO = USU.ID_USUARIO
LEFT JOIN PGC_CL.TSCR_TIPOS_MONEDAS MONEDA ON TSGE_MINUTA.CDTIPO_MONEDA= MONEDA.CDTIPO_MONEDA
LEFT JOIN PGC_CL.TSGE_NIVEL_APROBACION NIVEL ON TSGE_MINUTA.ID_NIVEL_APROBACION = NIVEL.ID_NIVEL_APROBACION
LEFT JOIN PGC_CL.TSGE_MINUTA_TOMADOR MINTOM ON MINTOM.ID_MINUTA = PGC_CL.TSGE_MINUTA.ID_MINUTA
LEFT JOIN PGC_CL.TSGE_MINUTA_APROBACION APRO ON APRO.ID_MINUTA = PGC_CL.TSGE_MINUTA.ID_MINUTA
WHERE /*MINU.USU_REGISTRA NOT IN ('AMANZANARES','LGARCIA','ETEJEDA','JCHAVEZ','SMILLA') AND*/ TSGE_MINUTA.FEC_REGISTRA >= TO_DATE('01/03/2018', 'DD/MM/YY')
AND TSGE_MINUTA.USU_REGISTRA <> 'MIGRACION-AMARU-CHILE'
-- AND (TSGE_MINUTA.COD_ESTADO = 'AP' OR TSGE_MINUTA.COD_ESTADO = 'RC')
AND catcob.NOM_DATO <> 'FIDELIDAD FUNCIONARIA'
AND catcob.NOM_DATO <> 'PERMANENCIA DE EMPLEADOS PUBLICOS'
ORDER BY 1, 7
"""
df = pd.read_sql(query,con)
#FORMATEAR LAS FECHAS CORRECTAMENTE
df['FEC_REGISTRA']=pd.to_datetime(df['FEC_REGISTRA'],format = '%d/%m/%Y %H:%M:%S %p')

#######
#APLICAR FECHA FORMATO DATASTUDIO
anhos=pd.DatetimeIndex(df['FEC_REGISTRA']).year.tolist()
meses=pd.DatetimeIndex(df['FEC_REGISTRA']).month.tolist()
dias=pd.DatetimeIndex(df['FEC_REGISTRA']).day.tolist()

anhoMes=["{}{:02}".format(anhos_, meses_) for anhos_, meses_ in zip(anhos, meses)]
anhoMesDia=["{}{:02}".format(anhoMes_, dias_) for anhoMes_, dias_ in zip(anhoMes, dias)]
######

df['PERIODO']=anhoMesDia
df.to_sql('Comite_GarantiasT_ResolucionRiesgos',mysql,chunksize=10000,if_exists='replace',index=False)
#####################

#%%
#IMPORTAR DATA RESOLUCIÓN RIESGOS GARANTIAS TÉCNICAS PARA CALCULO DE TIEMPOS DE RESPUESTA
query = """SELECT 
  DISTINCT PGC_CL.TSGE_MINUTA.ID_MINUTA,
 CASE WHEN PGC_CL.TSGE_MINUTA.COD_ESTADO = 'AP' THEN 'APROBADA'
      WHEN PGC_CL.TSGE_MINUTA.COD_ESTADO = 'PR' THEN 'PREREGISTRADA'
      WHEN PGC_CL.TSGE_MINUTA.COD_ESTADO = 'RE' THEN 'REGISTRADA'
      WHEN PGC_CL.TSGE_MINUTA.COD_ESTADO = 'PE' THEN 'PENDIENTE DE APROBACION'
      WHEN PGC_CL.TSGE_MINUTA.COD_ESTADO = 'RC' THEN 'RECHAZADA'
      WHEN PGC_CL.TSGE_MINUTA.COD_ESTADO = 'OB' THEN 'OBSERVADA'
      WHEN PGC_CL.TSGE_MINUTA.COD_ESTADO = 'CE' THEN 'CERRADA'
      ELSE 'INACTIVA'
 END AS ESTADO_MINUTA,
 MONEDA.DSTIPO_MONEDA,
 TOM.DNI,
 PGC_CL.TSGE_MINUTA.VAL_CONTRATO,
 MINDET.VAL_OTORGADO AS VALOR_OTORGADO,
 NVL(mincob.PORCENTAJE_TASA, 0) AS TASA,
 APRO.NOM_CARGO,
 PGC_CL.TSGE_MINUTA.USU_MODIFICA,
 --mincob.COD_TIPO_COBERTURA as COBERTURA,
 --catcob.NOM_DATO as COBERTURA,
 TOM.DSNOMBRES AS TOMADOR,
 CASE WHEN UPPER(USU.COD_USUARIO) IS NOT NULL THEN UPPER(USU.COD_USUARIO)
 ELSE PGC_CL.TSGE_MINUTA.USU_REGISTRA END AS SUSCRIPTOR,
 CASE WHEN PGC_CL.TSGE_MINUTA.COD_TIPO_MODALIDAD = 'LI' THEN 'LINEA'
      WHEN PGC_CL.TSGE_MINUTA.COD_TIPO_MODALIDAD = 'OP' THEN 'OPERACION PUNTUAL'
      ELSE PGC_CL.TSGE_MINUTA.COD_TIPO_MODALIDAD END AS TIPO_MODALIDAD,
 (CASE 
 WHEN PGC_CL.TSGE_MINUTA.COD_TIPO_MINUTA = 'S' THEN 'SUSCRIPCION' 
 WHEN PGC_CL.TSGE_MINUTA.COD_TIPO_MINUTA = 'C' THEN 'COMERCIAL' 
 ELSE 'RAVLA' END ) TIPO_MINUTA,
 NVL(NIVEL.NOM_NIVEL_APROBACION, 'MIGRADO') AS NIVEL_APROBACION,
 NVL(PGC_CL.TSGE_MINUTA.ID_PROYECTO, 'N/A') AS PROYECTO,
 TO_CHAR(MINDET.FEC_INICIO_VIGENCIA, 'DD/MM/YYYY') AS INICIO_VIGENCIA,
 TO_CHAR(MINDET.FEC_FIN_VIGENCIA, 'DD/MM/YYYY') AS FIN_VIGENCIA,
 MINDET.NUM_DIAS AS DIAS_PROYECTO,
 --NVL(MINDET.PORCENTAJE_TASA, 0) AS TASA,
 NVL(PGC_CL.TSGE_MINUTA.PORCENTAJE_FACULTATIVO, 0) AS PCT_FACULTATIVO_OPERACION,
 PGC_CL.TSGE_MINUTA.USU_REGISTRA,
 PGC_CL.TSGE_MINUTA.COD_TIPO_CANAL,
 --,(SELECT FEC_ACEPTACION FROM TSGE_PROPUESTA WHERE ID_PROPUESTA = )
 TO_CHAR(PGC_CL.TSGE_MINUTA.FEC_REGISTRA, 'dd/MM/yyyy HH:MI:SS AM') AS FEC_REGISTRA,
 --TO_CHAR(MINU.FEC_REGISTRA, 'dd/MM/yyyy HH:MI:SS AM') AS FEC_EMISION,
 TO_CHAR(NVL(PGC_CL.TSGE_MINUTA.FEC_MODIFICA, SYSDATE), 'dd/MM/yyyy HH:MI:SS AM') AS FEC_MODIFICA,
 TO_CHAR(NVL(PGC_CL.TSGE_MINUTA.FEC_RESOLUCION, SYSDATE), 'dd/MM/yyyy HH:MI:SS AM') AS FEC_RESOLUCION,
 MINTOM.VAL_CUPO_ALCANZAR CUMULO_A_ALCANZAR
 --PGC_CL.TSGE_MINUTA.MATERIA_ASEGURADA,
 --PGC_CL.TSGE_MINUTA.DESCRIP_COMENTARIO
 FROM PGC_CL.TSGE_MINUTA
 INNER JOIN PGC_CL.TSGE_MINUTA_DETALLE MINDET ON MINDET.ID_MINUTA = PGC_CL.TSGE_MINUTA.ID_MINUTA
 left join PGC_CL.TSGE_MINUTA_COBERTURA mincob on mincob.ID_MINUTA = PGC_CL.TSGE_MINUTA.ID_MINUTA
 left join PGC_CL.TSGE_CATALOGO_GENERAL catcob on (mincob.COD_TIPO_COBERTURA = catcob.COD_DATO and catcob.NOM_GRUPO = 'TIPO_COBERTURA') 
 --LEFT JOIN TCUM_TOMADORES TOM ON PGC_CL.TSGE_MINUTA.NMSEC_DNI_TOMADOR = TOM.NMSEC_DNI_TOMADOR
 LEFT JOIN PGC_CL.TSCR_PERSONAS TOM ON PGC_CL.TSGE_MINUTA.NMSEC_DNI_TOMADOR = TOM.NMSEC_DNI
 --LEFT JOIN PGC_PE.TCUM_CONSORCIOS CON ON PGC_CL.TSGE_MINUTA.NMCONSORCIO = CON.NMCONSORCIO
 LEFT JOIN PGC_CL.TSGE_USUARIO USU ON PGC_CL.TSGE_MINUTA.ID_USUARIO = USU.ID_USUARIO
 LEFT JOIN PGC_CL.TSCR_TIPOS_MONEDAS MONEDA ON PGC_CL.TSGE_MINUTA.CDTIPO_MONEDA= MONEDA.CDTIPO_MONEDA
 LEFT JOIN PGC_CL.TSGE_NIVEL_APROBACION NIVEL ON PGC_CL.TSGE_MINUTA.ID_NIVEL_APROBACION = NIVEL.ID_NIVEL_APROBACION
 LEFT JOIN PGC_CL.TSGE_MINUTA_TOMADOR MINTOM ON MINTOM.ID_MINUTA = PGC_CL.TSGE_MINUTA.ID_MINUTA
 LEFT JOIN PGC_CL.TSGE_MINUTA_APROBACION APRO ON APRO.ID_MINUTA = PGC_CL.TSGE_MINUTA.ID_MINUTA
 WHERE /*PGC_CL.TSGE_MINUTA.USU_REGISTRA NOT IN ('AMANZANARES','LGARCIA','ETEJEDA','JCHAVEZ','SMILLA') AND*/ PGC_CL.TSGE_MINUTA.FEC_REGISTRA >= TO_DATE('01/03/2018', 'DD/MM/YY')
 --and PGC_CL.TSGE_MINUTA.ID_MINUTA like '%M301201%'
ORDER BY 1, 7
"""
df = pd.read_sql(query,con)
#FORMATEAR LAS FECHAS CORRECTAMENTE
df['FEC_REGISTRA']=pd.to_datetime(df['FEC_REGISTRA'],format = '%d/%m/%Y %I:%M:%S %p')
df['FEC_RESOLUCION']=pd.to_datetime(df['FEC_RESOLUCION'],format = '%d/%m/%Y %I:%M:%S %p')
df['FEC_MODIFICA']=pd.to_datetime(df['FEC_MODIFICA'],format = '%d/%m/%Y %I:%M:%S %p')
#######
#APLICAR FECHA FORMATO DATASTUDIO
anhos=pd.DatetimeIndex(df['FEC_REGISTRA']).year.tolist()
meses=pd.DatetimeIndex(df['FEC_REGISTRA']).month.tolist()
dias=pd.DatetimeIndex(df['FEC_REGISTRA']).day.tolist()

anhoMes=["{}{:02}".format(anhos_, meses_) for anhos_, meses_ in zip(anhos, meses)]
anhoMesDia=["{}{:02}".format(anhoMes_, dias_) for anhoMes_, dias_ in zip(anhoMes, dias)]
######

df['PERIODO']=anhoMesDia
df.to_sql('Comite_GarantiasT_ResolucionRiesgos_Base_TiemposDeResolucion',sqlServer,chunksize=10000,if_exists='replace',index=False)

print('Se cargo todo')

#%%
#Fin 700 proveedores

query="""
with Libro as (
	select Libro,
	TipoOpeId
	from  [dim_TipoOperacionLibroLegal]
	left join [dim_Libro] on [dim_TipoOperacionLibroLegal].[LibroId]=[dim_Libro].[LibroId]
)

select distinct Empresa,
	[FechaEmision],
	[FechaVenc],
	[TotalConv],
    -- Iva conv duplica valores
	--[IvaConv],
	RazonSocial,
	[NumDoc],
    -- Este RUT esta incompleto, hay que obtenerlo de  dim_Entidad
    -- Rec_RutEmisor,
    dim_Entidad.Rut,
	Libro.Libro

FROM [dwh_avla].[dbo].[th_LibrosHonComVta]
left join dim_Empresa on th_LibrosHonComVta.Empid=dim_Empresa.Empid
left join [dim_Entidad] on th_LibrosHonComVta.EntId=dim_Entidad.EntId
left join Libro on th_LibrosHonComVta.TipoOpeId=Libro.TipoOpeId
"""


connStr = pyodbc.connect("""DRIVER={SQL Server Native Client 11.0};
                            SERVER=52.67.163.28;
                            DATABASE=dwh_avla;
                            UID=gestion;
                            PWD=gestion.19""")

df = pd.read_sql(query,connStr)
#Agregar el periodo de datastudio y arreglar el formato del rut
df["PeriodoDStudio"]=df["FechaEmision"].astype(str).str[:4]+df["FechaEmision"].astype(str).str[5:7]+'01'
df["Rut"]=df["Rut"].str.lstrip('0')
df['Rut']=df['Rut'].str.replace('-','')

clasificaciones=pd.read_excel('C:/Users/jjvalenzuela.AVALCHILE/Desktop/Clasificaciones Proveedores.xlsx')
clasificaciones["Rut"]=clasificaciones["Rut"].astype(str)
clasificaciones["Rut"] = clasificaciones["Rut"].str.upper()
df=df.merge(right=clasificaciones[["Rut","Clasificacion"]],how='left',on='Rut')
df["Clasificacion"]=df["Clasificacion"].where(pd.notnull(df["Clasificacion"]),"Otros")

df.to_sql('FacturasFin700',mysql,chunksize=10000,if_exists='replace',index=False)

connStr.close()

#%%
#Codigos SII SegCred
query="""select prod.nmpoliza poliza
             , a.cdactividad
          from (select nmpoliza
                from PGC_CL.tscr_reportes_ventas
                group by nmpoliza
                ) prod
            left join PGC_CL.tscr_polizas       pol on pol.nmpoliza    = prod.nmpoliza
            left join PGC_CL.tscr_personas      p   on p.nmsec_dni     = pol.nmsec_dni_asegurado
            left join PGC_CL.tscr_asegurados    a   on a.nmsec_dni_asegurado = p.nmsec_dni"""
df = pd.read_sql(query,con)
df.to_sql('SegCred_Codigo_SII',sqlServer,chunksize=10000,if_exists='replace',index=False)

#%%
##Capital Expuesto
#query= """ Select
#  trv.feproceso as fecha_proceso,
#  trv.nmpoliza numero_poliza,
#  substr(pers.dni,2,11) rut_asegurado,
#  pers.dsnombres nombre_asegurado,
#  trv.feinicio_vigencia fecha_inicio,
#  trv.fefin_vigencia fecha_termino,
#  substr(per.dni,2,11) rut_deudor,
#  per.dsnombres nombre_deudor,
#  trv.dsfactura_venta factura,
#  trv.fefactura fecha_factura,
#  pag.nmdias plazo,
#  trv.poprima tasa,
#  PGC_CL.monedaC(trv.nmpoliza) moneda,
#  trv.ptnotificado venta,
#  trv.ptprima prima,
#  case when PGC_CL.monedaC(trv.nmpoliza)='UF' then trv.ptnotificado*27625.4               ELSE  trv.ptnotificado*662.27 END  venta_pesos, --cambiar tipo de cambio
#  case when PGC_CL.monedaC(trv.nmpoliza)='UF' then trv.ptprima*27625.4 ELSE  trv.ptprima*662.27 END  prima_pesos, --cambiar tipo de cambio
#  cla.clasificacion clasificacion,
#  tin.dsnombres corredor
#  from PGC_CL.tscr_reportes_ventas trv
#  left join PGC_CL.tscr_polizas pol on trv.nmpoliza = pol.nmpoliza
#  left join PGC_CL.tscr_personas pers on pol.nmsec_dni_tomador = pers.nmsec_dni
#  left join PGC_CL.tscr_personas per on trv.nmsec_dni_deudor = per.nmsec_dni
#  left join PGC_CL.tscr_plazos_pagos pag on trv.cdplazo = pag.cdplazo
#  left join PGC_CL.tscr_deudores deu on per.nmsec_dni = deu.nmsec_dni_deudor
#  left join PGC_CL.tscr_clasificaciones cla on deu.nmsec_clasificacion = cla.nmsec_clasificacion
#  left join PGC_CL.tscr_interm_x_poliza tip on pol.nmpoliza = tip.nmpoliza
#  left join PGC_CL.tscr_intermediarios tin on tip.cdintermediario = tin.cdintermediario
#  where     pol.nmpoliza = trv.nmpoliza and to_char(trv.feproceso,'yyyymm') BETWEEN '201705' AND '201904'
#  -- and trv.nmsec_dni_deudor=216317
#   order by 1;
#"""


#query= """ Select 
#  tscr_reportes_ventas.feproceso as fecha_proceso,
#  tscr_reportes_ventas.nmpoliza numero_poliza,
#  substr(pers.dni,2,11) rut_asegurado,
#  pers.dsnombres nombre_asegurado,
#  tscr_reportes_ventas.feinicio_vigencia fecha_inicio,
#  tscr_reportes_ventas.fefin_vigencia fecha_termino,
#  substr(per.dni,2,11) rut_deudor,
#  per.dsnombres nombre_deudor,
#  tscr_reportes_ventas.dsfactura_venta factura,
#  tscr_reportes_ventas.fefactura fecha_factura,
#  pag.nmdias plazo,
#  tscr_reportes_ventas.poprima tasa,
#  PGC_CL.monedaC(tscr_reportes_ventas.nmpoliza) moneda,
#  tscr_reportes_ventas.ptnotificado venta,
#  tscr_reportes_ventas.ptprima prima,
#  case when PGC_CL.monedaC(tscr_reportes_ventas.nmpoliza)='UF' then tscr_reportes_ventas.ptnotificado*27625.4               ELSE  tscr_reportes_ventas.ptnotificado*662.27 END  venta_pesos, --cambiar tipo de cambio
#  case when PGC_CL.monedaC(tscr_reportes_ventas.nmpoliza)='UF' then tscr_reportes_ventas.ptprima*27625.4 ELSE  tscr_reportes_ventas.ptprima*662.27 END  prima_pesos, --cambiar tipo de cambio
#  cla.clasificacion clasificacion,
#  tin.dsnombres corredor
#  from PGC_CL.tscr_reportes_ventas
#  left join PGC_CL.tscr_polizas pol on tscr_reportes_ventas.nmpoliza = pol.nmpoliza
#    left join PGC_CL.tscr_polizas pol on tscr_reportes_ventas.nmpoliza = pol.nmpoliza
#  left join PGC_CL.tscr_personas pers on pol.nmsec_dni_tomador = pers.nmsec_dni
#  left join PGC_CL.tscr_personas per on tscr_reportes_ventas.nmsec_dni_deudor = per.nmsec_dni
#  left join PGC_CL.tscr_plazos_pagos pag on tscr_reportes_ventas.cdplazo = pag.cdplazo
#  left join PGC_CL.tscr_deudores deu on per.nmsec_dni = deu.nmsec_dni_deudor
#  left join PGC_CL.tscr_clasificaciones cla on deu.nmsec_clasificacion = cla.nmsec_clasificacion
#  left join PGC_CL.tscr_interm_x_poliza tip on pol.nmpoliza = tip.nmpoliza
#  left join PGC_CL.tscr_intermediarios tin on tip.cdintermediario = tin.cdintermediario
#  where     pol.nmpoliza = tscr_reportes_ventas.nmpoliza and to_char(tscr_reportes_ventas.feproceso,'yyyymm') BETWEEN '201904' AND '201905'
#"""
#
#
#
#df = pd.read_sql(query,conSC)



#%%
#Bases para join poliza - industria
#######################################
##TABLAS ANTIGUAS
#POLIZAS
query=""" select * from PGC_CL.TCUM_POLIZAS"""
df = pd.read_sql(query,con)
df.to_sql('TCUM_POLIZAS',sqlServer,chunksize=10000,if_exists='replace',index=False)

#Beneficiarios
query=""" select * from PGC_CL.TCUM_BENEFICIARIOS"""
df = pd.read_sql(query,con)
df.to_sql('TCUM_BENEFICIARIOS',sqlServer,chunksize=10000,if_exists='replace',index=False)

#Propuestas
query=""" select * from PGC_CL.TCUM_PROPUESTAS"""
df = pd.read_sql(query,con)
df.to_sql('TCUM_PROPUESTAS',sqlServer,chunksize=10000,if_exists='replace',index=False)

#Solicitudes
query=""" select * from PGC_CL.TCUM_SOLICITUDES"""
df = pd.read_sql(query,con)
df.to_sql('TCUM_SOLICITUDES',sqlServer,chunksize=10000,if_exists='replace',index=False)

#Tomadores
query=""" select * from PGC_CL.TCUM_TOMADORES"""
df = pd.read_sql(query,con)
df.to_sql('TCUM_TOMADORES',sqlServer,chunksize=10000,if_exists='replace',index=False)
###########################################################################

#POLIZAS
query=""" select * from PGC_CL.TSGE_POLIZA"""
df = pd.read_sql(query,con)
df.to_sql('TSGE_POLIZA',sqlServer,chunksize=10000,if_exists='replace',index=False)

#Actividades economicas
query=""" select * from PGC_CL.TSCR_ACTIVIDADES_ECO"""
df = pd.read_sql(query,con)
df.to_sql('TSCR_ACTIVIDADES_ECO',sqlServer,chunksize=10000,if_exists='replace',index=False)

#Sectores economicos
query=""" select * from PGC_CL.TSCR_SECTORES_ECO"""
df = pd.read_sql(query,con)
df.to_sql('TSCR_SECTORES_ECO',sqlServer,chunksize=10000,if_exists='replace',index=False)







