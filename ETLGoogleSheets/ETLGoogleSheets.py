from __future__ import print_function
import pickle
import os
import pandas as pd
import re
from sqlalchemy import create_engine
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request

sqlServer = create_engine("mssql+pyodbc://python:python@SqlServer")

#%%
os.chdir("C:/Users/jjvalenzuela.AVALCHILE/Documents/Python Scripts/ETLGoogleSheets")
def eliminarExprCelda(celda,expr,replace): #try catch de apoyo para incorporar dentro del apply en la funcion "eliminarExpr"
    try:
        expr=re.compile(str(expr))
        celda=re.sub(expr,replace,celda)
    except:
        pass
    return(celda)
    
def eliminarExpr(df,expr,replace): #Busca dentro de un dataframe una expresion y la reemplaza
    df=df.apply(lambda x: x.apply(eliminarExprCelda,args=(expr,replace)))
    return(df)
    
def VaciosaNone(df): #Funcion para buscar nan y nat dentro de un dataframe y convertirlos en vacios
    #Cambia los nan y NaT a Vacios, no sirve el pd.where para cambiar los NaT a Vacios
    df=df.apply(lambda x: x.apply(lambda y: '' if y != y else y ),axis=0)
    df=df.where((pd.notnull(df)),'')
    return(df)

def makeService(SCOPES):
    creds = None
    # The file token.pickle stores the user's access and refresh tokens, and is
    # created automatically when the authorization flow completes for the first
    # time.
    if os.path.exists('token.pickle'):
        with open('token.pickle', 'rb') as token:
            creds = pickle.load(token)
    # If there are no (valid) credentials available, let the user log in.
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(
                'credentials.json', SCOPES)
            creds = flow.run_local_server()
        # Save the credentials for the next run
        with open('token.pickle', 'wb') as token:
            pickle.dump(creds, token)

    service = build('sheets', 'v4', credentials=creds)
    return(service)

def uploadData(df,sheetId,batch,service,sheet=""):
    if sheet != "":
        sheet=sheet + "!"
    df=df.fillna('')
    #nombres de la columna
    dfnames=df.columns.values.tolist()
    #convertir timestamps a texto para poder convertir en json
    for i in range(0,len(dfnames)):
        if df[dfnames[i]].dtype.num==21 or df[dfnames[i]].dtype.num == 17:
            df[dfnames[i]]=df[dfnames[i]].astype(str)
        if df[dfnames[i]].dtype.num==12:
            print(dfnames[i])
            df[dfnames[i]]=df[dfnames[i]].astype(str)
            df[dfnames[i]]=df[dfnames[i]].apply(eliminarExprCelda,args=("\\.",","))
      
    #Llenar los vacios
    
    df=df.astype(str)
    
    #convertir dataframe en lista de listas para meterlas en un json
    df=df.values.tolist()
    inicioBatch=2 #De donde parte el bloque a incorporar
    for finBatch in range(0,len(df)+1):
        if (finBatch % batch) == 0:
            #diccionario con la info a subir
            body = {'values': df[inicioBatch:finBatch]}
            #agregar cuerpo de la tabla
            #rango de input
            start= sheet+'R'+str(inicioBatch+2)+'C1'
            end= 'R'+str(finBatch+3)+'C'+str(len(dfnames))
            range_name=start + ':' + end 
            value_input_option = 'USER_ENTERED'
            result = service.spreadsheets().values().update(
                spreadsheetId=sheetId, range=range_name,valueInputOption=value_input_option, body=body).execute()
            inicioBatch=finBatch #Valor inicio proximo batch
            print("se han subido "+str(finBatch+1)+" datos")
            
        elif finBatch==len(df):
            #diccionario con la info a subir
            body = {'values': df[inicioBatch:finBatch]}
            #agregar cuerpo de la tabla
            #rango de input
            start= sheet+'R'+str(inicioBatch+2)+'C1'
            end= 'R'+str(finBatch+3)+'C'+str(len(dfnames))
            range_name=start + ':' + end 
            value_input_option = 'USER_ENTERED'
            result = service.spreadsheets().values().update(
                spreadsheetId=sheetId, range=range_name,valueInputOption=value_input_option, body=body).execute()  
            print("se han subido "+str(finBatch+1)+" datos")
    #Agregar header de la tabla
    start= sheet+'R1C1'
    end= 'R1'+'C'+str(len(dfnames))
    range_name=start + ':' + end
    
    body = {'values': [dfnames]}
    
    value_input_option = 'USER_ENTERED'
    result = service.spreadsheets().values().update(
        spreadsheetId=sheetId, range=range_name,valueInputOption=value_input_option, body=body).execute()
    return(result)

#%%
# If modifying these scopes, delete the file token.pickle.
SCOPES = ['https://www.googleapis.com/auth/spreadsheets']
service= makeService(SCOPES)

#sheet para testear
sheetId='1v4ffaXkbcoVqXemFAf21OxoMYE3S_5bNtODo5vr5ljE'

#Datos para testear
query = 'select * from SuscripcionGarantiasTecnicas'
#realizar la query
df = pd.read_sql(query,sqlServer)

sheet="Hoja 2"
result=uploadData(df,sheetId,1000,service,sheet)

range_ = 'A:BN' # TODO: asdasdsa
clear_values_request_body = {
    # TODO: Add desired entries to the request body.
}

request = service.spreadsheets().values().clear(spreadsheetId=sheetId, range=range_, body=clear_values_request_body)
response = request.execute()









