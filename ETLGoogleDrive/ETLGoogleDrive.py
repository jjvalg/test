
from __future__ import print_function
import pickle
import os
import pandas as pd
from sqlalchemy import create_engine
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
from googleapiclient.http import MediaIoBaseDownload
import io

sqlServer = create_engine("mssql+pyodbc://python:python@SqlServer")


os.chdir("C:/Users/fevalenzuela/Documents/Python Scripts/ETLGoogleDrive")

#%%
def makeService(SCOPES):
    """Shows basic usage of the Drive v3 API.
    Prints the names and ids of the first 10 files the user has access to.
    """
    creds = None
    # The file token.pickle stores the user's access and refresh tokens, and is
    # created automatically when the authorization flow completes for the first
    # time.
    if os.path.exists('token.pickle'):
        with open('token.pickle', 'rb') as token:
            creds = pickle.load(token)
    # If there are no (valid) credentials available, let the user log in.
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(
                'credentials.json', SCOPES)
            creds = flow.run_local_server()
        # Save the credentials for the next run
        with open('token.pickle', 'wb') as token:
            pickle.dump(creds, token)

    service = build('drive', 'v3', credentials=creds)
    return(service)
    

SCOPES = ['https://www.googleapis.com/auth/drive']
service= makeService(SCOPES)
#lp g
file_id = '1x9fB3_Gw3fb70sqkTPAGzIJSP5RJF87N'
request = service.files().get_media(fileId=file_id)
filename='test'
fh = io.FileIO(filename, 'wb')
downloader = MediaIoBaseDownload(fh, request)
done = False
while done is False:
    status, done = downloader.next_chunk()
    print("Download %d%%." % int(status.progress() * 100))
    
fh.close()
    

#lp cr
file_id = '1oExB-7kieBDrIr5vogibXOlnjQsgSxuW'
request = service.files().get_media(fileId=file_id)
filename='LpPeruCr.xlsx'
fh = io.FileIO(filename, 'wb')
downloader = MediaIoBaseDownload(fh, request)
done = False
while done is False:
    status, done = downloader.next_chunk()
    print("Download %d%%." % int(status.progress() * 100))
    
fh.close()


